﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAODAL.Model;

namespace CodeFirstCore.UnitOfWork
{
    class Controller
    {
        public void transactionController()
        {
            UnitOfWork unitOfWork = new UnitOfWork(new AdventureContext());

            unitOfWork.UserRepository.Add(new User
            {
                Name = "SamUnitOfWork",
                Email = "sam@unitofwork.com"
            });
            unitOfWork.Save();

            unitOfWork.AccountRepository.Add(new Account
            {
                UserId = 17,
                AccountNumber = 66515217,
                Balance = 100017
            });

            unitOfWork.Save();
        }
    }
}
