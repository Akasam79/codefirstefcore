﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAODAL.Model;

namespace CodeFirstCore.UnitOfWork
{
    class AccountRepository
    {
        private AdventureContext context;

        public AccountRepository(AdventureContext adventureContext)
        {
            context = adventureContext;
        }

        public void Add(Account contxt)
        {
            context.Accounts.Add(contxt);
        }

        public void Delete(Account contxt)
        {
            context.Accounts.Remove(contxt);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
