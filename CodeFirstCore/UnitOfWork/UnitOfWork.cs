﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAODAL.Model;

namespace CodeFirstCore.UnitOfWork
{
    class UnitOfWork
    {
        private UserRepository userRepository;
        private AccountRepository accountRepository;
        private AdventureContext adventureContext;

        public UnitOfWork (AdventureContext adventureContext)
        {
            this.adventureContext = adventureContext;
        }

        public UserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(adventureContext);
                }
                return userRepository;
            }

        }

        public AccountRepository AccountRepository
        {
            get
            {
                if(accountRepository == null)
                {
                    accountRepository = new AccountRepository(adventureContext);
                }
                return accountRepository;
            }
        }

        public void Save()
        {
            adventureContext.SaveChanges();
        }
    }
}
