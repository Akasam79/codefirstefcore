﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAODAL.Model;
using System.Data.Entity;


namespace CodeFirstCore.UnitOfWork
{
    class UserRepository
    {
        private AdventureContext context = new AdventureContext();

        public UserRepository(AdventureContext adventureContext)
        {
            context = adventureContext;
        }
        public void Add(User contxt)
        {
            context.Users.Add(contxt);
        }

        public void Delete(User contxt)
        {
            context.Users.Remove(contxt);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

    }
}
