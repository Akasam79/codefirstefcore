﻿using System;
using BEZAODAL.Model;
using CodeFirstCore.UnitOfWork;
using CodeFirstCore.CRUD;
using CodeFirstCore.DataInitialization;

namespace CodeFirstCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AdventureContext();
            Controller controller = new Controller();
            //MyDataInitializer.RecreateDatabase(context);
            //MyDataInitializer.InitializeUsers(context);
            //MyDataInitializer.InitializeAccount(context);


            //User[] users2add =
            //{
            //    new User ()
            //    {
            //        Name = "name1",
            //        Email = "email1"
            //    },
            //    new User ()
            //    {
            //        Name = "name2",
            //        Email = "email2"
            //    },
            //    new User ()
            //    {
            //        Name = "name3",
            //        Email = "email3"
            //    }
            //};
            //CrudAndTransaction.AddMany(users2add);

            //CrudAndTransaction.UpdateUser(12);

            //CrudAndTransaction.RemoveRecord(17);

            //CrudAndTransaction.AddNewRecord();

            //CrudAndTransaction.MakeTransaction(1, 12, 500);

            //controller.transactionController();

            CrudAndTransaction.SelectAll();

            Console.WriteLine("All complete");
            Console.ReadLine();
        }
    }
}
