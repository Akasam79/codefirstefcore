﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAODAL.Model;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstCore.DataInitialization
{
    public static class MyDataInitializer
    {
        public static void InitializeUsers(AdventureContext context)
        {
            var AddUsers = new List<User>
            {
                new User {Name = "Tochukwu", Email = "Tochukwu@gmail.com"},
                new User {Name = "Shola", Email = "shola.nejo@domain.com" },
                new User {Name = "Chikky", Email = "chikky@domain.com" },
                new User {Name = "Dara", Email = "dara@domain.com" },
                new User { Name = "Alex", Email = "alex@domain.com" },
                new User { Name = "Uriel", Email = "Uriel@domain.com" },
                new User { Name = "Kachi", Email = "kachi@domain.com" },
                new User { Name = "Chinedu", Email = "chinedu@domain.com" },
                new User { Name = "Loveth", Email = "loveth@domain.com" },
                new User { Name = "Sunday", Email = "sunday@domain.com" },
                new User { Name = "Sammy", Email = "sammy@domain.com" },
                new User { Name = "Einstien", Email = "einstien @domain.com" },
                new User { Name = "KCM", Email = "kcm@domain.com" },
                new User { Name = "Obinna", Email = "obi@domain.com" },
                new User { Name = "Gideon", Email = "Giddy@domain.com" },
                new User { Name = "Francis", Email = "Francis2@domain.com" },
            };
            AddUsers.ForEach(x => context.Users.Add(x));
            context.SaveChanges();
        }

        public static void InitializeAccount(AdventureContext context)
        {
            var addAccount = new List<Account>
            {
                new Account {UserId = 1, AccountNumber =  66515201, Balance = 100001},
                new Account {UserId = 2, AccountNumber =  66515202, Balance = 100002},
                new Account {UserId = 3, AccountNumber =  66515203, Balance = 100003},
                new Account {UserId = 4, AccountNumber =  66515204, Balance = 100004},
                new Account {UserId = 5, AccountNumber =  66515205, Balance = 100005},
                new Account {UserId = 6, AccountNumber =  66515206, Balance = 100006},
                new Account {UserId = 7, AccountNumber =  66515207, Balance = 100007},
                new Account {UserId = 8, AccountNumber =  66515208, Balance = 100008},
                new Account {UserId = 9, AccountNumber =  66515209, Balance = 100009},
                new Account {UserId = 10, AccountNumber = 66515210, Balance = 100010},
                new Account {UserId = 11, AccountNumber = 66515211, Balance = 100011},
                new Account {UserId = 12, AccountNumber = 66515212, Balance = 100012},
                new Account {UserId = 13, AccountNumber = 66515213, Balance = 100013},
                new Account {UserId = 14, AccountNumber = 66515214, Balance = 100014},
                new Account {UserId = 15, AccountNumber = 66515215, Balance = 100015},
                new Account {UserId = 16, AccountNumber = 66515216, Balance = 100016},
            };
            addAccount.ForEach(y => context.Accounts.Add(y));
            context.SaveChanges();

        
        }
            

        public static void RecreateDatabase(AdventureContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.Migrate();
        }

        public static void ClearData(AdventureContext context)
        {
            ExecuteDeleteSql(context, "User");
            ExecuteDeleteSql(context, "Account");
            ExecuteDeleteSql(context, "Transaction");
            ResetIdentity(context);
        }

        private static void ExecuteDeleteSql(AdventureContext context, string tableName)
        {
            //With 2.0, must separate string interpolation if not passing in params
            var rawSqlString = $"Delete from dbo.{tableName}";
            context.Database.ExecuteSqlRaw(rawSqlString);
        }

        private static void ResetIdentity(AdventureContext context)
        {
            var tables = new[] { "Users", "Accounts", "Transactions" };
            foreach (var itm in tables)
            {
                //With 2.0, must separate string interpolation if not passing in params
                var rawSqlString = $"DBCC CHECKIDENT (\"dbo.{itm}\", RESEED, -1);";
                context.Database.ExecuteSqlRaw(rawSqlString);
            }
        }
    }
}
