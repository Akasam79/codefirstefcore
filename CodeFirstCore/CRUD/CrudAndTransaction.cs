﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAODAL.Model;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstCore.CRUD
{
    class CrudAndTransaction
    {
        public static void AddNewRecord()
        {
            using (var contxt = new AdventureContext())
            {
                var user = new User()
                {
                    Name = "Ebuka",
                    Email = "NewEmail3"
                };
                contxt.Users.Add(user);
                contxt.SaveChanges();
            }
        }

        public static void AddMany(IEnumerable<User> UserToAdd)
        {
            using (var context = new AdventureContext())
            {
                context.Users.AddRange(UserToAdd);
                context.SaveChanges();
            }
        }

        public static void SelectAll()
        {
            using (var context = new AdventureContext())
            {
                foreach (User user in context.Users)
                {
                    Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
                }
            }
        }

        public static void UpdateUser(int userId)
        {

            var user = new User()
            {
                Name = "Einstein",
                Email = "Einstein@newmail.com"
            };
            using (var context = new AdventureContext())
            {
                User userToUpdate = context.Users.Find(userId);

                if (userToUpdate != null)
                {
                    userToUpdate.Name = user.Name;
                    userToUpdate.Email = user.Email;

                    if (context.Entry(userToUpdate).State != EntityState.Modified)
                    {
                        throw new Exception("Unable to Modify record o, no vex");
                    }
                    context.SaveChanges();
                    Console.WriteLine("Record has been updated o, Ok, see what we have now if you don't believe me.....");
                }

                else
                {
                    Console.WriteLine();
                    Console.WriteLine($"\t \b user with id {userId} not found");
                }
            }
        }

        public static void RemoveRecord(int userId)
        {
            using (var context = new AdventureContext())
            {
                User userToDelete = context.Users.Find(userId);

                if (userToDelete != null)
                {
                    context.Users.Remove(userToDelete);

                    Account accountToDelete = context.Accounts.Find(userId);
                    if(accountToDelete != null)
                    {
                        context.Accounts.Remove(accountToDelete);
                    }

                    if (context.Entry(userToDelete).State != EntityState.Deleted || context.Entry(accountToDelete).State != EntityState.Deleted)
                    {
                        throw new Exception("Unable to delete record o, no vex");
                    }

                    context.SaveChanges();

                    Console.WriteLine("Record has been deleted o, Ok, see what we have now if you don't believe me.....");
                }

                else
                {
                    Console.WriteLine();
                    Console.WriteLine($"\t \b user with id {userId} not found");
                }
            }
        }

        public static void MakeTransaction(int senderId, int receiverId, int amount)
        {
            using (var context = new AdventureContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var receiver = context.Accounts.Where(c => c.Id == senderId).FirstOrDefault();
                        receiver.Balance -= amount;
                        context.SaveChanges();

                        var sender = context.Accounts.Where(c => c.Id == receiverId).FirstOrDefault();
                        sender.Balance += amount;
                        context.SaveChanges();

                        var updateTransaction = new Transaction()
                        {
                            UserId = senderId,
                            Mode = "transfer",
                            ReceiverId = receiverId,
                            Amount = amount,
                            Time = DateTime.Now
                        };
                        context.Transactions.Add(updateTransaction);
                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Console.WriteLine("Error occured " + ex);
                    }
                }
            }
        }
    }
}
