﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BEZAODAL.Model
{
    [Index(nameof(UserId), Name = "IX_UserId")]
    public partial class Account
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Column("Account_Number")]
        public int AccountNumber { get; set; }
        [Column(TypeName = "decimal(38, 2)")]
        public decimal Balance { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("Accounts")]
        public virtual User User { get; set; }
    }
}
