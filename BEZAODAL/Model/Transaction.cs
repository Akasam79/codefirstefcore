﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BEZAODAL.Model
{
    [Index(nameof(UserId), Name = "IX_UserId1")]
    public partial class Transaction
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        [StringLength(10)]
        public string Mode { get; set; }
        public int ReceiverId { get; set; }
        [Column(TypeName = "decimal(38, 2)")]
        public decimal Amount { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Time { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("Transactions")]
        public virtual User User { get; set; }
    }
}
